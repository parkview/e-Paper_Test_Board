# ePaper Test Board 
  
When testing or working with a new ePaper, it's handy to see whats going on with the ePaper lines/pins. It would be handy to have a 2-way flex cable board that sits between the ePaper controller and the ePaper.  This has a series of 0603 status LEDs that are powered from the flex cable.  The status LEDs will light up letting you know that the flex cable was wired up and plugged in correctly, as well as SPI and various ePaper voltages are ok.

  
For more documentation visit [https://gitlab.com/parkview/e-Paper_Test_Board](https://gitlab.com/parkview/e-Paper_Test_Board)
  
## Hardware Features:
---------
#### Test Board: 

* sits in between (in-line) with the ePaper controller and the ePaper display
* all 24 pins are labeled and broken out into a 0.1" header 
* status LEDs on most of the 24 pins
* can see various voltage levels and pulses passing through to ePaper 
  
  
## Available Software 
---------------
![Gitlab URL](images/ePaper-Test-Board_QR_URL.png){ align=right : style="height:100px;width:100px"}
* no special software needed to use this Test board  
  
  
![RGB Controller V0.8](images/ePaper-Test-Board.jpg){ align=left : style="height:200px;width:600px"}  
