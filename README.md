Project Name: e-Paper Test Board  
-----------
Date: 2022-06-21  
  

Had Greg over this morning and we where discussing how we could test a e-Paper connection.  Greg had included some Testpoints under the connector.  I previously had included test points scattered around my initial ePaper circuit.  

It would be handy to make a 2-way flex cable board, that has a series of 0603 LEDs that are powered from the flex cable.  With the right MCU program driving the e-Paper circuit, and a GND jumpered to the LED cathodes, they would light up letting you know that the flex cable was wired up and plugged in correctly.  
  
Plugging in the e-Paper would then fire up the special + 1 lines of the e-Paper power supply and the corrosponding LEDs would light up as well.  
  
Kind of a 2 step test approch:  
1) test main MCU GPIO and 3.3V power lines, with out the e-paper pluggged in
2) tests the e-Paper power circuitry

Make a board and release the design as Open Source for other people to build and use.






24 Pin connector:
-----------

e-Paper Present:  
 1: NC  
 2: GDR - MOSFET Gate control  
 3: RESE - current sense  
 4: VGL - negative gate driving voltage  
 5: VGH - positive gate driving voltage  
 6: TSCL - i2C clock Temp sensor  
 7: i2C data - temperature sensor  
 8: BSI - Bus selction pin  
 9: Busy out  
10: RES - Reset input, active low  
11: D/C - data/command control input  
12: CS - chip select  
13: D0 - SPI clock  
14: D1 - SPI data  
15: VDDIO - voltage for interface logic  
16: VCI - voltage for internal IC  
17: VSS - Ground  
18: VDD - voltage for core power  
19: VPP - voltage for OTP programming  
20: VSH - driving source voltage  
21: PREVGH - Power Supply pin for VGH and VSH  
22: VSL - negative source driving voltage  
23: PREVGL - Power Supply pin for VCOM, VGL and VSLe  
24: VCOM - VCOM driving voltage  
  


PCB Versions:
-------------

V0.5 Production: JLCPCB, 2 layer, 1.6mm Green  
Date: 2022-06-24  
 
* uploaded the single row version  
* note: Red LED is labled wrong and could be fixed with a new PCB version  




Project Journal:
----------------

2022-07-21:  
laser cut plastic solder stencil and assembled and tested PCB.  
 
2022-06-24:  
re-checked layout and uploaded single breakout pin row design to JLCPCB. Updated Gitlab repo. Ordered the 24pin flex connectors from LCSC.  
 
2022-06-23:  
started PCB layout. Found I had incorrectly laid out schematic with reversed connector pins and it needs to be redrawn. Added in LCSC parts into BOM file.  
  
2022-06-22:  
measured a 2.7" e-Paper connector with it in and out of circuit.  Took screenshots where necessary and entered data into spreadsheet. Uploaded to Git repo.  
  
2022-06-21:  
initial project idea and discussion with Greg.  Create initial schematic design in KiCAD. Setup GitLab project repo.  
















































